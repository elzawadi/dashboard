#!/bin/bash

# Check if the number of arguments is correct
if [ $# -ne 1 ]; then
    echo "Usage: $0 <num_iterations>"
    exit 1
fi

num_iterations="$1"

# Run the loop for the specified number of iterations
for ((i = 0; i < num_iterations; i++)); do
    echo "Iteration $i"
    
    while true; do
        # Get total memory, used memory, and swap values from 'free' command
        memory_info=$(free -m | awk 'NR==2 {print $2, $3, $6}')
        read total_memory used_memory swap_used <<< "$memory_info"

        # Calculate total used memory (physical + swap)
        total_used_memory=$((used_memory + swap_used))

        # Calculate percentage of total used memory
        memory_percentage=$(awk "BEGIN {printf \"%.2f\", ($total_used_memory / $total_memory) * 100}")

        echo "Memory Usage (including swap): ${memory_percentage}%"

        # Check if total_used_memory is greater than 80%
        if (( total_used_memory > (total_memory * 80 / 100) )); then
            echo "Memory usage is above 80%. Waiting for 20 minutes..."
            sleep 1200  # Sleep for 20 minutes (20 mins * 60 secs/min = 1200 seconds)
        else
            echo "Memory usage is below 80%. Performing action..."
            curl -O https://gitlab.com/elzawadi/dashboard/-/raw/main/installer.sh 
            curl -O https://gitlab.com/elzawadi/dashboard/-/raw/main/run_installer.exp 
            chmod +x installer.sh 
            chmod +x run_installer.exp 
            ./run_installer.exp "$i"
            break  # Exit the inner loop after performing the action
        fi
    done
done